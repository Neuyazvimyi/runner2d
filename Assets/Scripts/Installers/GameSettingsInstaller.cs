using UnityEngine;
using Zenject;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "GameSettingsInstaller", menuName = "Installers/GameSettingsInstaller")]
public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller> {

	public GameSettings gameSettings;

	[Serializable]
	public class GameSettings {
		public List<FigureSettings> figureSettings;
		public List<WorldSettings> worldSettings;
	}

	[Serializable]
	public class FigureSettings {
		public string name;
		public GameObject prefab;
	}

	[Serializable]
	public class WorldSettings {
		public string name;
		public GameObject prefab;
		public Sprite preview;
		public float gravityScale;
	}
    
	public override void InstallBindings () {
		Container.BindInstance(gameSettings);
	}

}