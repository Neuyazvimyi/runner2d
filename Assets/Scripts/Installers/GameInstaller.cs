using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller<GameInstaller> {

//	[Inject]
//	Games _settings = null;
    
	public override void InstallBindings () {
		Container.BindInstance<GameInstaller> (this).AsSingle ().NonLazy ();
    }

	public GameObject InstantiatePrefab (GameObject go) {
		return Container.InstantiatePrefab (go);
	}

}