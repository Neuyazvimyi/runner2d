﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;

public class GameWorld : MonoBehaviour {

	public List<GameObject> worldParts;
	public GameObject currentPart;
	public GameObject previousPart;

	private int checkPos = 100;
	private int partLength = 100;

	private Vector3 posVector;

	// Use this for initialization
	void Start () {
		
	}

	public void Initialize (float gravityScale) {
		Physics2D.gravity = Vector2.down * gravityScale;
	}

	public void CheckWorld (float posX) {
		if (posX > checkPos - 50) {
			if (previousPart != null) {
				previousPart.SetActive (false);
			}

			posVector = currentPart.transform.localPosition;
			posVector.x += partLength;

			var partsList = worldParts.FindAll (x => !x.activeSelf);
			GameObject newPart = partsList[Random.Range(0, partsList.Count)];
			newPart.transform.localPosition = posVector;
			newPart.SetActive (true);

			previousPart = currentPart;
			currentPart = newPart;

			checkPos += partLength;
		}
	}


}
