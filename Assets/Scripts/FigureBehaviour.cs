﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Zenject;

public class FigureBehaviour : MonoBehaviour {

	public Rigidbody2D rigidBody;
	public SpriteRenderer spriteRenderer;
	public float runSpeed;
	public float jumpForce;
	public float jumpTorque;
	public float groundCheckDistance;
	public float maxSpeed;
	public float speedStep;
	public Color jumpColor;

	public ReactiveProperty<bool> isDead { get; private set; }
	public ReactiveProperty<bool> isJumping { get; private set; }

	private Vector3 groundVector;
	private Vector2 velocityVector;
	private CameraFollow cameraFollow;
	private Color mainColor;

	[Inject]
	public void Construct (CameraFollow cameraFollow) {
		this.cameraFollow = cameraFollow;
	}

	void Awake () {
		isDead = new ReactiveProperty<bool> (false);
		isJumping = new ReactiveProperty<bool> (false);
		mainColor = spriteRenderer.color;
	}

	public void Initialize () {
		cameraFollow.Follow (transform);

		Observable.EveryUpdate ()
			.Where (_ => Input.GetMouseButtonDown (0))
			.Subscribe (_ => {
				Jump ();
			}).AddTo (this);

		isJumping.ObserveEveryValueChanged (x => x.Value)
			.Subscribe (xs => {
				if (xs == true) {
					spriteRenderer.color = jumpColor;
				} else {
					spriteRenderer.color = mainColor;
				}
			}).AddTo (this);

		SetVelocity();
	}

	private void Jump () {
		if (IsGrounded() && !isDead.Value) {
			rigidBody.AddForce (Vector2.up * jumpForce);
			rigidBody.AddTorque (jumpTorque);
			isJumping.Value = true;
		}
	}

	private void OnDeath () {
		rigidBody.bodyType = RigidbodyType2D.Static;
	}

	private bool IsGrounded () {
		groundVector = transform.position;
		groundVector.y -= groundCheckDistance;
		return Physics2D.Linecast(transform.position, groundVector, 1 << LayerMask.NameToLayer("Ground"));
	}

	private void SetVelocity () {
		velocityVector = rigidBody.velocity;
		velocityVector.x = runSpeed;
		rigidBody.velocity = velocityVector;
	}

	public virtual void AddSpeed () {
		SetVelocity ();
	}

	void OnCollisionEnter2D (Collision2D coll) {
		if (coll.gameObject.tag == "Obstacle") {
			isDead.Value = true;
			OnDeath ();
		} else {
			isJumping.Value = false;
		}
	}

}
