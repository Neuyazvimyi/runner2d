﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UnityEngine.UI;
using UniRx;

public class ViewFragmentFigure : MonoBehaviour {

	public Button upButton;
	public Button downButton;
	public Text currentFigureText;

	private GameSettingsInstaller.GameSettings gameSettings;
	public ReactiveProperty<int> currentIndex { get; private set; } 

	[Inject]
	public void Construct (GameSettingsInstaller.GameSettings gameSettings) {
		this.gameSettings = gameSettings;
	}

	// Use this for initialization
	void Start () {
		var figureSettings = gameSettings.figureSettings;
		currentIndex = new ReactiveProperty<int> (0);

		currentIndex.ObserveEveryValueChanged (x => x.Value)
			.Subscribe (xs => {
				currentFigureText.text = figureSettings[xs].name;
			}).AddTo (this);

		upButton.OnClickAsObservable ().Subscribe (_ => {
			if (currentIndex.Value < figureSettings.Count-1) {
				currentIndex.Value++;
			} else {
				currentIndex.Value = 0;
			}
		}).AddTo (this);

		downButton.OnClickAsObservable ().Subscribe (_ => {
			if (currentIndex.Value > 0) {
				currentIndex.Value--;
			} else {
				currentIndex.Value = figureSettings.Count-1;
			}
		}).AddTo (this);
	}

}
