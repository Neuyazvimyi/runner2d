﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class CameraFollow : MonoBehaviour {

	public float damping = 5.0f;
	public float distanceOffset;

	private Transform target;
	private Vector3 camPos;

	public void Follow (Transform target) {
		this.target = target;

		Observable.EveryLateUpdate ()
			.Subscribe (_ => {
				EveryLateUpdate ();
			}).AddTo (target.gameObject);
	}

	private void EveryLateUpdate () {
		camPos = Vector3.Lerp(transform.position, target.position, damping);
		camPos.y = transform.position.y;
		camPos.z = transform.position.z;

		camPos.x += distanceOffset;

		transform.position = camPos;
	}
}
