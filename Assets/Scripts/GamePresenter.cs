﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Zenject;

public class GamePresenter : MonoBehaviour {

	public Transform parentTransform;
	private GameObject figureGo;
	private GameObject gameWorldGo;
	public CompositeDisposable disposables;

	private FigureBehaviour figureBehaviour;
	private GameWorld gameWorld;
	private int bestScore;

	public ReactiveProperty<GameState> currentState { get; private set; }
	public enum GameState {
		Menu,
		Game,
		End
	}

	private GameView gameView;
	private GameInstaller gameInstaller;
	private ViewFragmentFigure viewFragmentFigure;
	private ViewFragmentWorld viewFragmentWorld;
	private GameSettingsInstaller.GameSettings gameSettings;

	[Inject]
	public void Construct (
		GameView gameView,
		GameInstaller gameInstaller,
		ViewFragmentFigure viewFragmentFigure,
		ViewFragmentWorld viewFragmentWorld,
		GameSettingsInstaller.GameSettings gameSettings)
	{
		this.gameView = gameView;
		this.gameInstaller = gameInstaller;
		this.viewFragmentFigure = viewFragmentFigure;
		this.viewFragmentWorld = viewFragmentWorld;
		this.gameSettings = gameSettings;
	}

	// Use this for initialization
	void Start () {
		Initialize ();
//		OnLoadGame ();
		bestScore = 0;
	}

	void OnEnable () {
		disposables = new CompositeDisposable();
	}

	void OnDisable () {
		if (disposables != null) {
			disposables.Dispose ();
		}
	}

	private void Initialize () {
		currentState = new ReactiveProperty<GameState> (GameState.Menu);

		currentState.ObserveEveryValueChanged (x => x.Value)
			.Subscribe (_ => {
				OnStateChange ();
			}).AddTo (this);

		gameView.playButton.OnClickAsObservable ()
			.Subscribe (_ => {
				currentState.Value = GameState.Game;
			}).AddTo (this);

		gameView.menuButton.OnClickAsObservable ()
			.Subscribe (_ => {
				disposables.Dispose ();
				disposables = new CompositeDisposable ();
				OnLoadGame ();
				currentState.Value = GameState.Menu;
			}).AddTo (this);

		gameView.restartButton.OnClickAsObservable ()
			.Subscribe (_ => {
				disposables.Dispose ();
				disposables = new CompositeDisposable ();
				OnLoadGame ();
				currentState.Value = GameState.Game;
			}).AddTo (this);

	}

	private void OnLoadGame () {
		if (gameWorld != null) {
			Destroy (gameWorld.gameObject);
		}
		if (figureBehaviour != null) {
			Destroy (figureBehaviour.gameObject);
		}

		int index = viewFragmentFigure.currentIndex.Value;
		figureGo = gameSettings.figureSettings[index].prefab;
		index = viewFragmentWorld.currentIndex.Value;
		gameWorldGo = gameSettings.worldSettings[index].prefab;

		gameWorld = gameInstaller.InstantiatePrefab (gameWorldGo).GetComponent<GameWorld> ();
		figureBehaviour = gameInstaller.InstantiatePrefab (figureGo).GetComponent<FigureBehaviour> ();

		gameWorld.Initialize (gameSettings.worldSettings[index].gravityScale);

		gameWorld.transform.SetParent (parentTransform);
		figureBehaviour.transform.SetParent (parentTransform);
	}

	private void OnMenu () {
		gameView.RenderBestScore (bestScore);
	}

	private void OnGame () {
		Observable.Timer (System.TimeSpan.FromSeconds (1))
			.RepeatSafe ()
			.Subscribe (_ => {
				if (!figureBehaviour.isDead.Value) {
					figureBehaviour.AddSpeed ();
				}
			}).AddTo (disposables);

		Observable.Timer (System.TimeSpan.FromSeconds (0.25D))
			.RepeatSafe ()
			.Subscribe (_ => {
				if (!figureBehaviour.isDead.Value) {
					float posX = figureBehaviour.transform.localPosition.x;
					gameWorld.CheckWorld (posX);
					gameView.RenderGameScore (posX);
				}
			}).AddTo (disposables);

		figureBehaviour.isDead.ObserveEveryValueChanged (x => x.Value)
			.Subscribe (xs => {
				if (xs == true) {
					currentState.Value = GameState.End;
				}
			}).AddTo (disposables);
				
		figureBehaviour.Initialize ();
	}

	private void OnEnd () {
		float posX = figureBehaviour.transform.localPosition.x;

		if ((int)posX > bestScore) {
			bestScore = (int)posX;
		}

		gameView.RenderEndScore (posX);
		gameView.RenderEndBestScore (bestScore);
	}

	private void OnStateChange () {
		switch (currentState.Value) {
		case GameState.Menu:
			gameView.menuPanel.SetActive (true);
			gameView.gamePanel.SetActive (false);
			gameView.endPanel.SetActive (false);
			OnMenu ();
			break;
		case GameState.Game:
			gameView.menuPanel.SetActive (false);
			gameView.gamePanel.SetActive (true);
			gameView.endPanel.SetActive (false);
			OnLoadGame ();
			OnGame ();
			break;
		case GameState.End:
			gameView.menuPanel.SetActive (false);
			gameView.gamePanel.SetActive (false);
			gameView.endPanel.SetActive (true);
			OnEnd ();
			break;
		}
	}

}
