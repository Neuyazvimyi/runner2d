﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameView : MonoBehaviour {

	public GameObject menuPanel;
	public GameObject gamePanel;
	public GameObject endPanel;

	public Button playButton;
	public Text bestScoreText;

	public Text gameScoreText;

	public Button menuButton;
	public Button restartButton;
	public Text endScoreText;
	public Text endBestScoreText;

	// Use this for initialization
	void Start () {
		
	}

	public void RenderBestScore (int score) {
		bestScoreText.text = "Best: " + score;
	}

	public void RenderGameScore (float score) {
		gameScoreText.text = ((int)score).ToString();
	}

	public void RenderEndScore (float score) {
		endScoreText.text = ((int)score).ToString();
	}

	public void RenderEndBestScore (int score) {
		endBestScoreText.text = "Best: " + score;
	}

}
