﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using Zenject;

public class ViewFragmentWorld : MonoBehaviour {

	public Button leftButton;
	public Button rightButton;
	public Text currentWorldText;
	public Image currentPreview;

	private GameSettingsInstaller.GameSettings gameSettings;
	public ReactiveProperty<int> currentIndex { get; private set; }

	[Inject]
	public void Construct (GameSettingsInstaller.GameSettings gameSettings) {
		this.gameSettings = gameSettings;
	}

	// Use this for initialization
	void Start () {
		var worldSettings = gameSettings.worldSettings;
		currentIndex = new ReactiveProperty<int> (0);

		currentIndex.ObserveEveryValueChanged (x => x.Value)
			.Subscribe (xs => {
				currentWorldText.text = worldSettings[xs].name;
				currentPreview.sprite = worldSettings[xs].preview;
			}).AddTo (this);

		leftButton.OnClickAsObservable ().Subscribe (_ => {
			if (currentIndex.Value < worldSettings.Count-1) {
				currentIndex.Value++;
			} else {
				currentIndex.Value = 0;
			}
		}).AddTo (this);

		rightButton.OnClickAsObservable ().Subscribe (_ => {
			if (currentIndex.Value > 0) {
				currentIndex.Value--;
			} else {
				currentIndex.Value = worldSettings.Count-1;
			}
		}).AddTo (this);
	}

}
