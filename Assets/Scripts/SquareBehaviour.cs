﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareBehaviour : FigureBehaviour {

	public override void AddSpeed () {
		if (this.maxSpeed > this.runSpeed) {
			this.runSpeed += this.speedStep;
		}
		base.AddSpeed ();
	}

}
